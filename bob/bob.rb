class Bob

def self.hey(remark)
  remark = remark.to_s

  if remark.upcase == remark && remark.end_with?("?") && remark.match?(/^[0-9?]*$/) == false && remark.match?(":") == false
    answer = "Calm down, I know what I'm doing!"
  elsif remark.gsub(' ','').empty?
    answer = "Fine. Be that way!"
  elsif remark.gsub("\t",'').empty?
    answer = "Fine. Be that way!"
  elsif remark.gsub("\n\r \t",'').empty?
    answer = "Fine. Be that way!"
  elsif remark.match? (/^[0-9 ,]*$/)
    answer = "Whatever."
  elsif remark.gsub(' ', '').end_with?("?")
    answer = 'Sure.'
  elsif remark.upcase == remark
    answer = "Whoa, chill out!"
  elsif remark.end_with?("?")
    answer = 'Sure.'
  else
    answer = "Whatever."
  end
  return answer
end
end

module BookKeeping
  VERSION = 2
end
