class Phrase

  def initialize(string)
    @string = string.downcase
  end

  def word_count
    @string.gsub! /[!@#$%^&*():.,\n]/ , ' '
    array = @string.split(' ')
    hash = {}
    array.each do |i|
      if i.start_with?("'")
        i.gsub! "'", ''
        hash[i] = array.count(i)
      else
        hash[i] = array.count(i)
      end
    end
    return hash
  end

end

module BookKeeping
  VERSION = 1
end
