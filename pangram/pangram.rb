class Pangram

  def self.pangram?(string)
    array = ('a'...'z').to_a
    string.downcase.split('').each {|i| array.delete(i)}
    array.empty?
  end

end

module BookKeeping
  VERSION = 6
end
