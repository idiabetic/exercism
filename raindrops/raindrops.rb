class Raindrops

  def self.convert(number)
    map = {3 => 'Pling',
          5 => 'Plang',
          7 => 'Plong'}
    string = ''
    if number % 3 == 0 || number % 5 == 0 || number % 7 == 0
      map.each do |key, value|
        if number % key == 0
          string << value
        end
      end
    else
      string = number.to_s
    end
    return string
  end
end

module BookKeeping
   VERSION = 3
end
