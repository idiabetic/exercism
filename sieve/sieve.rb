class Sieve

  def initialize(number)
    @number = number

  end

  def primes
    array = (2..@number).to_a
    index = 0
    multiplier = 2

    until array[index] == nil
      until (array[index] * multiplier) > @number
        array.delete(array[index] * multiplier)
        multiplier = multiplier + 1
      end
      index = index + 1
      multiplier = 2
    end
    return array
  end
end

module BookKeeping
  VERSION = 1 # Where the version number matches the one in the test.
end
