
class Squares
  def initialize(count)
    @numbers = 0.upto(count)
  end

  def sum_of_squares
    @sum_of_squares ||= @numbers.reduce { |sum, number| sum + (number ** 2) }
  end

  def square_of_sum
    @square_of_sums ||= @numbers.reduce(:+) ** 2
  end

  def difference
    square_of_sum - sum_of_squares
  end
end

module BookKeeping
   VERSION = 4
end
