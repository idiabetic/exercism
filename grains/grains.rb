class Grains


  def self.square(square)
    fail ArgumentError, "There is no square #{square}" if square < 1 || square > 64
    array = [1]
    (1..64).each {|i| array << (array[i - 1] * 2)}
    array[square - 1]
  end

  def self.total
    array = [1]
    (1..64).each {|i| array << (array[i - 1] * 2)}
    array.pop
    array.sum
  end
end


module BookKeeping
  VERSION = 1
end
