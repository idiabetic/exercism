class Hamming
  def self.compute(string1, string2)
    score = 0
    if string1.length == string2.length
      string1.split('').zip(string2.split('')).each do |val1, val2|
        if val1 != val2
          score += 1
        end
      end
      return score
    else
      raise ArgumentError.new("Input of different lenghts")
    end
  end
end

module BookKeeping
   VERSION = 3
end
