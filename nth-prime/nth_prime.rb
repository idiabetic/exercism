require 'prime'

class Prime
  def self.nth(nth)
    if nth < 1
      fail ArgumentError, "Can't be a Negative Int"
    else
      take(nth).last
    end
  end
end

module BookKeeping
  VERSION = 1
end
