class Integer

  def to_roman
    map = {
      1000 => 'M',
      900 => 'CM',
      500 => 'D',
      400 => 'CD',
      100 => 'C',
      90 => 'XC',
      50 => 'L',
      40 => 'XL',
      10 => 'X',
      9 => 'IX',
      5 => 'V',
      4 => 'IV',
      1 => 'I',
    }

    return '' if self == 0
    number = self
    str = ''
    map.each do |key, value|
      result = number / key
      number = number % key
      str << (value * result)
    end
    return str
  end
end

module BookKeeping
  VERSION = 2
end
