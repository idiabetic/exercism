class Year

  def self.leap?(year)
    if year % 4 == 0
      if  year % 100 == 0 && year % 400 != 0
        leap = false
      else
        leap = true
      end
    end
    return leap
  end
end

module BookKeeping
  VERSION = 3
end
